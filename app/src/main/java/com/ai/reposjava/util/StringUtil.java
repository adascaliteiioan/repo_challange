package com.ai.reposjava.util;

import android.util.Base64;

import java.io.UnsupportedEncodingException;

public class StringUtil {

    public static String decodeFromBase64(String text) {
        String t = "";
        if (text == null) {
            return t;
        }
        byte[] data = Base64.decode(text, Base64.DEFAULT);
        try {
            t = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return t;
    }
}
