package com.ai.reposjava.network;

import com.ai.reposjava.models.Readme;
import com.ai.reposjava.models.SearchResult;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitService {

    @GET("search/repositories?sort=stars&order=desc")
    Single<Response<SearchResult>> getReposByKey(@Query("q") String key, @Query("page") int page);

    @GET("/repos/{owner}/{repo}/readme")
    Single<Response<Readme>> getReadme(@Path("owner") String owner, @Path("repo") String repo);

}
