package com.ai.reposjava.ui.repos;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ai.reposjava.R;
import com.ai.reposjava.databinding.RepoItemBinding;
import com.ai.reposjava.models.Repo;
import com.bumptech.glide.Glide;

import java.util.List;

public class ReposAdapter extends RecyclerView.Adapter<ReposAdapter.ReposHolder> {

    private List<Repo> repos;
    private LoadData loadData;
    private ItemClickListener itemClick;

    @NonNull
    @Override
    public ReposHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RepoItemBinding binding = RepoItemBinding.inflate(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        return new ReposHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ReposHolder reposHolder, int i) {
        reposHolder.bind(repos.get(i));

        if (i == repos.size() - 1) {
            loadData.onDataLoaded();
        }


    }

    @Override
    public int getItemCount() {
        return repos != null ? repos.size() : 0;
    }

    public void updateData(List<Repo> repos) {
        this.repos = repos;
        notifyDataSetChanged();
    }

    public void addData(List<Repo> repos) {
        this.repos.addAll(repos);
        notifyDataSetChanged();
    }

    public void setItemClickListener(ItemClickListener itemClick) {
        this.itemClick = itemClick;
    }

    public void setLoadData(LoadData loadData) {
        this.loadData = loadData;
    }

    public class ReposHolder extends RecyclerView.ViewHolder {
        private RepoItemBinding binding;

        public ReposHolder(@NonNull RepoItemBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }

        public void bind(Repo repo) {
            binding.setRepo(repo);
            itemView.setOnClickListener(v -> itemClick.onItemClicked(repo));
            Glide.with(itemView.getContext())
                    .load(repo.getOwner().getAvatar())
                    .placeholder(R.drawable.ic_photo_size_select_actual_black_24dp)
                    .fitCenter()
                    .into(binding.ownerAvatar);
        }
    }
}
