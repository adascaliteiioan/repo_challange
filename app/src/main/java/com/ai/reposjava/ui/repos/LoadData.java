package com.ai.reposjava.ui.repos;

public interface LoadData {
    void onDataLoaded();
}
