package com.ai.reposjava.ui.repos;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;

import com.ai.reposjava.R;
import com.ai.reposjava.base.BaseActivity;
import com.ai.reposjava.databinding.ActivityMainBinding;
import com.ai.reposjava.models.Repo;
import com.ai.reposjava.ui.repodetails.RepoDetailsActivity;

import java.util.List;

public class MainActivity extends BaseActivity {

    private ReposViewModel viewModel;
    private ActivityMainBinding binding;
    private ReposAdapter adapter;

    public static final String REPO_KEY = "repo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        viewModel = ViewModelProviders.of(this).get(ReposViewModel.class);
        binding.setViewModel(viewModel);
        adapter = new ReposAdapter();
        binding.setAdapter(adapter);
        observe();
        handleData();
    }

    private void observe() {
        viewModel.getRepos().observe(this, this::onChanged);
    }

    private void handleData() {
        adapter.setLoadData(() -> viewModel.getRepos().observe(this, repoList -> {
            if (repoList != null) {
                Log.v("repos", repoList.size() + "");
                adapter.addData(repoList);
            }
        }));

        adapter.setItemClickListener(repo -> {
            Intent intent = new Intent(MainActivity.this, RepoDetailsActivity.class);
            intent.putExtra(REPO_KEY, repo);
            startActivity(intent);
        });

    }

    private void onChanged(List<Repo> repoList) {
        if (repoList != null) {
            Log.v("repos", repoList.size() + "");
            adapter.updateData(repoList);
        }


    }
}
