package com.ai.reposjava.ui.repos;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.ai.reposjava.models.Repo;
import com.ai.reposjava.repository.ReposRepository;

import java.util.List;

public class ReposViewModel extends AndroidViewModel {

    private ReposRepository repository;
    private int currentPage = 1;

    public ReposViewModel(@NonNull Application application) {
        super(application);
        repository = ReposRepository.getInstance(application);
    }

    LiveData<List<Repo>> getRepos() {
        return repository.getRepos(currentPage++);
    }


}
