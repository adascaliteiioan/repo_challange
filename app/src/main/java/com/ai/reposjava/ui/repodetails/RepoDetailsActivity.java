package com.ai.reposjava.ui.repodetails;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.ai.reposjava.R;
import com.ai.reposjava.base.BaseActivity;
import com.ai.reposjava.databinding.RepoDetailsActivityBinding;
import com.ai.reposjava.models.Repo;
import com.ai.reposjava.ui.repos.MainActivity;
import com.ai.reposjava.util.StringUtil;

public class RepoDetailsActivity extends BaseActivity {

    private RepoDetailsActivityBinding binding;
    private RepoDetailsViewModel detailsViewModel;
    private Repo repo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.repo_details_activity);
        detailsViewModel = ViewModelProviders.of(this).get(RepoDetailsViewModel.class);

        getExtras();
        observe();
    }

    private void getExtras() {
        if (getIntent() != null && getIntent().getExtras() != null) {
            repo = (Repo) getIntent().getExtras().get(MainActivity.REPO_KEY);
            detailsViewModel.setRepo(repo);
        } else {
            repo = detailsViewModel.getRepo();
        }
        if (repo == null) {
            repo = new Repo();
        }
        binding.setRepo(repo);
    }

    private void observe() {
        detailsViewModel.getReadme(repo.getOwner().getName(), repo.getName())
                .observe(this, readme -> {
                    String text = "N/A";
                    if (readme != null) {
                        text = readme.getContent().isEmpty() ? readme.getHtmlUrl() : StringUtil.decodeFromBase64(readme.getContent());
                    }
                    binding.contentReadme.setText(text);
                });

    }
}
