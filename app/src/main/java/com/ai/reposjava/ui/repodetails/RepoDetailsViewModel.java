package com.ai.reposjava.ui.repodetails;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.ai.reposjava.models.Readme;
import com.ai.reposjava.models.Repo;
import com.ai.reposjava.repository.ReposRepository;

public class RepoDetailsViewModel extends AndroidViewModel {

    private Repo repo;
    private ReposRepository repository;

    public RepoDetailsViewModel(@NonNull Application application) {
        super(application);
        repo = new Repo();
        repository = ReposRepository.getInstance(application);
    }

    public Repo getRepo() {
        return repo;
    }

    public void setRepo(Repo repo) {
        this.repo = repo;
    }

    public LiveData<Readme> getReadme(String owner, String repo) {
        return repository.getReadme(owner, repo);
    }
}
