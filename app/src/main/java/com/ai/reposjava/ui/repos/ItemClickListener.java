package com.ai.reposjava.ui.repos;

import com.ai.reposjava.models.Repo;

public interface ItemClickListener {

    void onItemClicked(Repo repo);
}
