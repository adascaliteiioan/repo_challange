package com.ai.reposjava.models;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;

public class RepoOwnerConverter {

    private static Gson gson = new Gson();
    @TypeConverter
    public static String ownerToString(RepoOwner repoOwner) {
        return gson.toJson(repoOwner);
    }

    @TypeConverter
    public static RepoOwner stringToOwner(String repo) {
        return gson.fromJson(repo, RepoOwner.class);
    }
}
