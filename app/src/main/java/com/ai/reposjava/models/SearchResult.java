package com.ai.reposjava.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.reactivex.annotations.Nullable;

public class SearchResult {

    @SerializedName("items")
    private List<Repo> repos;

    @Nullable
    public List<Repo> getRepos() {
        return repos;
    }

    public void setRepos(List<Repo> repos) {
        this.repos = repos;
    }
}
