package com.ai.reposjava.repository;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.util.Log;

import com.ai.reposjava.database.RepoDBHelper;
import com.ai.reposjava.models.Readme;
import com.ai.reposjava.models.Repo;
import com.ai.reposjava.models.SearchResult;
import com.ai.reposjava.network.RetrofitHelper;
import com.ai.reposjava.network.RetrofitService;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class ReposRepository {

    private static ReposRepository instance;
    private RetrofitService service;
    private RepoDBHelper repoDBHelper;

    private ReposRepository(Context context) {
        service = RetrofitHelper.getInstance().getService(RetrofitService.class);
        repoDBHelper = new RepoDBHelper(context);
    }

    public static ReposRepository getInstance(Context context) {
        if (instance == null) {
            instance = new ReposRepository(context);
        }
        return instance;
    }

    @SuppressLint("CheckResult")
    public LiveData<List<Repo>> getRepos(int page) {
        Single<Response<SearchResult>> responseSingle = service.getReposByKey("android", page);

        responseSingle.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(searchResult -> {
                    if (searchResult != null) {
                        if (searchResult.body() != null) {
                            repoDBHelper.save(searchResult.body().getRepos());
                        }
                    } else {
                        Log.v("Repository", "Error");
                    }
                }, throwable -> Log.v("Repository", throwable.getMessage()));

        return repoDBHelper.getAll();
    }

    @SuppressLint("CheckResult")
    public LiveData<Readme> getReadme(String owner, String repo) {
        Single<Response<Readme>> readme = service.getReadme(owner, repo);

        readme.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    if (result != null) {
                        Readme r = result.body();
                        if (r != null) {
                            r.setRepo(repo);
                            r.setOwner(owner);
                            repoDBHelper.save(r);
                            Log.v("readme", r.getContent());
                        }
                    }
                }, throwable -> Log.v("Repository", throwable.getMessage()));
        return repoDBHelper.getReadme(owner, repo);
    }
}
