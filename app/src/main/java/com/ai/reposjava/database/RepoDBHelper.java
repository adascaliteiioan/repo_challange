package com.ai.reposjava.database;

import android.arch.lifecycle.LiveData;
import android.content.Context;

import com.ai.reposjava.database.dao.ReadmeDao;
import com.ai.reposjava.database.dao.RepoDao;
import com.ai.reposjava.models.Readme;
import com.ai.reposjava.models.Repo;

import java.util.List;

import io.reactivex.annotations.Nullable;

public class RepoDBHelper extends BaseDBHelper {

    private RepoDao repoDao;
    private ReadmeDao readmeDao;

    public RepoDBHelper(Context context) {
        super(context);
        repoDao = appDB.getRepoDao();
        readmeDao = appDB.getReadmeDao();
    }

    public LiveData<List<Repo>> getAll() {
        return repoDao.getAll();
    }

    public void save(@Nullable List<Repo> repos) {
        if (repos != null) {
            repoDao.insert(repos);
        }
    }

    public void save(Readme readme) {
        readmeDao.insert(readme);
    }

    public LiveData<Readme> getReadme(String owner, String repo) {
        return readmeDao.getReadme(owner, repo);
    }
}
