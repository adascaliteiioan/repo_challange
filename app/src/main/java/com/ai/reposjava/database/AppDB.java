package com.ai.reposjava.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.ai.reposjava.database.dao.ReadmeDao;
import com.ai.reposjava.database.dao.RepoDao;
import com.ai.reposjava.models.Readme;
import com.ai.reposjava.models.Repo;

@Database(entities = {Repo.class, Readme.class}, version = 5, exportSchema = false)
public abstract class AppDB extends RoomDatabase {
    public static final String DATABASE_DB = "database.db";
    private static AppDB appDatabase;

    public static AppDB getInstance(Context context) {
        if (appDatabase == null) {
            appDatabase = Room.databaseBuilder(context, AppDB.class, DATABASE_DB)
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build();
        }
        return appDatabase;
    }

    public abstract RepoDao getRepoDao();
    public abstract ReadmeDao getReadmeDao();
}
