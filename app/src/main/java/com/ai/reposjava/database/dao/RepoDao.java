package com.ai.reposjava.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.ai.reposjava.models.Repo;

import java.util.List;

@Dao
public interface RepoDao extends BaseDao<Repo> {

    @Query("select * from Repo order by stars desc")
    LiveData<List<Repo>> getAll();
}
