package com.ai.reposjava.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.ai.reposjava.models.Readme;

@Dao
public interface ReadmeDao extends BaseDao<Readme> {

    @Query("select * from Readme where repo = :repo and owner = :owner")
    LiveData<Readme> getReadme(String owner, String repo);
}
