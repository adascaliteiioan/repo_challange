package com.ai.reposjava.database;

import android.content.Context;

public class BaseDBHelper {

    protected AppDB appDB;

    public BaseDBHelper(Context context) {
        appDB = AppDB.getInstance(context);
    }
}
